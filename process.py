import subprocess
import sys
import os
from server import *

class Process:
	
	def __init__(self):
		pass
	
	def execCommand(self,argv):
		self.out = ""
		argv = argv.rstrip().split(' ')
		self.processes = subprocess.Popen(['ps','-A'],stdout=subprocess.PIPE)
		self.lines, self.err = self.processes.communicate()
		if len(argv) == 1:
			if argv[0] == "view":
				self.viewprocesses()
		elif len(argv) == 2:
			if argv[0] == 'kill':
				self.killprocess(argv[1])
				
		else:
			self.out = "No actions!"
			
	def viewprocesses(self):
		self.out = ""
		for l in self.lines.splitlines():
			self.out+="\n"+str(l) 

	def killprocess(self,search):
		self.out = ""
		for l in self.lines.splitlines():
			if search in l.lower():
				process_id = int(l.split(None,1)[0])
				os.kill(process_id,9)	
				self.out = "Killed processes"
				
	def returnOutput(self):
		return self.out
		


