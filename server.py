from socket import *
from process import *
class ConnectionRecv:
	
	#initialize all connection variables
	# @server - the server address
	# @port - the port address
	# @number - the number of connections
	def __init__(self,server,port,number):
		self.serversocketa = socket(AF_INET,SOCK_STREAM)
		ADDRS = (server,port)
		self.serversocketa.bind(ADDRS)
		self.serversocketa.listen(number)
		self.p = Process()
	#accept connections
	# @buffersize - the size of the incoming message
	def acceptConnection(self,buffersize):
		while 1 :
			print "Waiting for connections.."
			self.clientsocketa, self.address = self.serversocketa.accept()
			print "connection from " , self.address
			while 1 :
				self.command = self.clientsocketa.recv(buffersize)
				if not self.command: 
					break
				if not self.command == "stop":
					print "Command : " + self.command
					self.p.execCommand(self.command)
					self.clientsocketa.send("Output :" + self.p.returnOutput())
				else:
					self.closeConnection()

	#close the connection				
	def closeConnection(self):
		self.clientsocketa.close()
		self.serversocketa.close()
		

class ConnectionSend:
	
	#initialize connection variable
	# @server - server address
	# @portnumber - port number to send and receive connections from
	# @buffersize - buffer size
	def __init__(self,server,portnumber):
		self.host = server
		self.port = portnumber
		self.addr = (self.host, self.port)
	
	#start connection
	def startConnection(self,buffersize):
		self.clientsocketa = socket(AF_INET, SOCK_STREAM)
		self.clientsocketa.connect(self.addr)
		self.buffer = buffersize
		while 1:
			data = raw_input('>')
			if not data: 
				break 
			self.clientsocketa.send(data)
			data = self.clientsocketa.recv(self.buffer)
			if not data: 
				break 
			print data
	
	#close connection
	def closeConnection(self):
		self.clientsocketa.close()